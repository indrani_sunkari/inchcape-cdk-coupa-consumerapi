package com.inchcape;

public class ObjectLine {
	
	private String wipNumber = "";
	private String wipLine = "";
	
	public String getWipNumber() {
		return wipNumber;
	}

	public void setWipNumber(String wipNumber) {
		this.wipNumber = wipNumber;
	}

	public String getWipLine() {
		return wipLine;
	}

	public void setWipLine(String wipLine) {
		this.wipLine = wipLine;
	}

}
