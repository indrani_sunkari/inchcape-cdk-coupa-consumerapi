package com.inchcape;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class PoCsvFileToXML { 

	public static String NEW_LINE_SEPARATOR = System.getProperty("line.separator"); 

	public static void main(String args[]) throws Exception {
		PoCsvFileToXML csvFileToXML = new PoCsvFileToXML();

		String filename = "src/test/resources/po-line-change3159.csv";
	    String str = new String(Files.readAllBytes(Paths.get(filename)));

		String data = csvFileToXML.convertPoCSVFileFormatToXML(str, ",");
		 
	     System.out.println(data);
 
	}
	 
	
	public String convertPoCSVFileFormatToXML(String csvData) throws Exception {
		return convertPoCSVFileFormatToXML(csvData, ",");
	}

	private String convertPoCSVFileFormatToXML(String csvData, String delimiter) throws Exception {

		if (csvData == null) {
			throw new Exception("csvData is null");
		}

		if (delimiter == null) {
			throw new Exception("delimiter is null");
		}

		String lines[] = csvData.split(NEW_LINE_SEPARATOR);

		int fieldCount = 0;
		String[] csvFields = null;
		StringTokenizer stringTokenizer = null;

		List<String[]> data = new ArrayList <String[]> ()  ;

		String curLine = "";
		int len = lines.length;

		for (int j = 1; j < len; j++) {
			curLine = lines[j];

			System.out.println(curLine);
			stringTokenizer = new StringTokenizer(curLine, delimiter);
			fieldCount = stringTokenizer.countTokens();
			csvFields = new String[fieldCount];
			//System.out.println("fieldCount:" + fieldCount);
			if (fieldCount > 0) {
				int i = 0;
				String str = "";
				while (stringTokenizer.hasMoreElements()) {
					String tmp = String.valueOf(stringTokenizer.nextElement());
                    str = tmp.replace("\"", "");			

					csvFields[i++] = str;
					str = "";
				}
			}
			data.add(csvFields);
		}
		// System.out.println (data.size());

		PoXMLGenerator xml = new PoXMLGenerator();
		return xml.createPoXML(data);

	}

}