package com.inchcape;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

class ObjectStatusXMLGenerator {
	private static final String SUBLET_BATCH = "SubletBatch";
	
	private static final String ID = "id";
	private static final String SUBLET_LINE = "subletLine";
	private static final String WIP_NO = "WIPNo";
	private static final String WIP_LINE = "WIPLine";
	private static final String ORDER_STATUS = "OrderStatus";
	private static final String INTEGRATION_MSG = "IntegrationMessage";
	private static final String STATUS_ERROR = "error";
	private static final String POS_COMPANY = "POSCompany";

	public List<String> transformReqToResponse(List<RequestPayload> responsePayloadList) throws Exception {
		List<String> respXmlList = new ArrayList();
		for (RequestPayload requestPayload : responsePayloadList) {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			// root elements
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement(SUBLET_BATCH);
			doc.appendChild(rootElement);
			Element idElement = doc.createElement(ID);
			String idStr = requestPayload.getBusinessId();
			if (idStr != null) {
				if (requestPayload.getPosCompany() != null) {
					idStr = idStr.replace("-" + requestPayload.getPosCompany(), "");
				}
			}
			idElement.appendChild(doc.createTextNode(idStr));
			rootElement.appendChild(idElement);

			List<ObjectLine> objectLineList = requestPayload.getObjectLine();
			for (ObjectLine objectLine : objectLineList) {
				Element subletLineElement = doc.createElement(SUBLET_LINE);
				rootElement.appendChild(subletLineElement);

				Element posCompanyElement = doc.createElement(POS_COMPANY);
				String posCompany = requestPayload.getPosCompany();
				if (posCompany != null) {
					String splitStr[] = posCompany.split("-");
					int len = splitStr.length;
					if (len == 2) { // e.g. CML-18, will get 18 only
						posCompany = splitStr[1];
					} else {
						// do nothing, set whole posCompany string;
					}
				}
				posCompanyElement.appendChild(doc.createTextNode(posCompany));
				subletLineElement.appendChild(posCompanyElement);

				Element wipNoElement = doc.createElement(WIP_NO);
				wipNoElement.appendChild(doc.createTextNode(objectLine.getWipNumber()));
				subletLineElement.appendChild(wipNoElement);

				Element wipLineElement = doc.createElement(WIP_LINE);
				String wipLine = objectLine.getWipLine();
				if (wipLine != null) {
					String splitStr[] = wipLine.split("-");
					int len = splitStr.length;
					if (splitStr.length == 3) { // e.g. WIP-LINE-10, will get 10
												// only
						wipLine = splitStr[2];
					} else {
						// do nothing, set whole posCompany string;
					}
				}
				wipLineElement.appendChild(doc.createTextNode(wipLine));
				subletLineElement.appendChild(wipLineElement);

				Element orderStatusElement = doc.createElement(ORDER_STATUS);
				String statusTmp = requestPayload.getStatus();
				if (statusTmp.equalsIgnoreCase(STATUS_ERROR)) {
					statusTmp = "8";
				} else {
					statusTmp = "2";
				}
				orderStatusElement.appendChild(doc.createTextNode(statusTmp));
				subletLineElement.appendChild(orderStatusElement);

				Element ingegrationMsgElement = doc.createElement(INTEGRATION_MSG);
				ingegrationMsgElement.appendChild(doc.createTextNode(requestPayload.getIntegraitonMsg()));
				subletLineElement.appendChild(ingegrationMsgElement);
			}

			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			transformer.setOutputProperty("omit-xml-declaration", "yes");
			DOMSource source = new DOMSource(doc);

			StreamResult result = new StreamResult(new StringWriter());

			transformer.transform(source, result);
			String xmlResult = result.getWriter().toString();
			respXmlList.add(xmlResult);
		}

		return respXmlList;
	}

}
