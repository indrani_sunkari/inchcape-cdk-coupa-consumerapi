package com.inchcape;
import java.util.List;

import com.inchcape.ObjectLine;

public class RequestPayload {
	private String businessId = "";
	private String posCompany = "";
	
	private String status= "";
	private String integraitonMsg= "";
	private List<ObjectLine> objectLine;

	public String getBusinessId() {
		return businessId;
	}

	public void setBusinessId(String businessId) {
		this.businessId = businessId;
	}

	public String getPosCompany() {
		return posCompany;
	}

	public void setPosCompany(String posCompany) {
		this.posCompany = posCompany;
	}
	

	public List<ObjectLine> getObjectLine() {
		return objectLine;
	}

	public void setObjectLine(List<ObjectLine> objectLine) {
		this.objectLine = objectLine;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIntegraitonMsg() {
		return integraitonMsg;
	}

	public void setIntegraitonMsg(String integraitonMsg) {
		this.integraitonMsg = integraitonMsg;
	}
}
