package com.inchcape;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class XMLGenerator {

	public String createXML(List<String[]> data) throws Exception {
		String orderNumberTmp = "";
		for (String reqData[] : data) {
			orderNumberTmp = reqData[4];
			break;
		}

		List<String[]> dataTmp = new ArrayList();
		int batchSize = 0;

		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		Document doc = docBuilder.newDocument();
		Element batchObjectRootElement = doc.createElement("batch-object");
		doc.appendChild(batchObjectRootElement);
		
		Element objectCollectionElement = doc.createElement("object-collection");
		batchObjectRootElement.appendChild(objectCollectionElement);

		for (String reqData[] : data) {
			String orderNumber = reqData[4];
			if (!orderNumberTmp.equals(orderNumber)) {

				createXMLDataDetails(doc,objectCollectionElement, dataTmp);
				batchSize = batchSize + 1;
				
				orderNumberTmp = orderNumber;
				dataTmp = new ArrayList();
				dataTmp.add(reqData);
				continue;
			}
			dataTmp.add(reqData);
		}

		batchSize = batchSize + 1;
		createXMLDataDetails(doc,objectCollectionElement, dataTmp);;
		 
		createXMLHeaderData (doc,batchObjectRootElement,batchSize+"" );
		
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC, "yes");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
		DOMSource source = new DOMSource(doc);

		StreamResult result = new StreamResult(new StringWriter());

		transformer.transform(source, result);
		String 	xmlResult = result.getWriter().toString();
		return xmlResult;
	}

	private void createXMLHeaderData(Document doc, Element batchObjectRootElement, String batchSizeStr) throws Exception {
		Element batchHeader = doc.createElement("batch-header");
		batchObjectRootElement.appendChild(batchHeader);

		Element objectType = doc.createElement("object-type");
		objectType.appendChild(doc.createTextNode("requisition"));
		batchHeader.appendChild(objectType);

		Element batchSize = doc.createElement("batch-size");
		batchSize.appendChild(doc.createTextNode(batchSizeStr));
		batchHeader.appendChild(batchSize);

		Element batchId = doc.createElement("batch-id");
		// put 12345 for now, need to be changed later when got clear mapping
		batchId.appendChild(doc.createTextNode("12345"));
		batchHeader.appendChild(batchId);

		Element filename = doc.createElement("filename");
		// put sg-bmsl-cdk-requisition-20190617144535.xml for now, need to be
		// changed later when got clear mapping
		filename.appendChild(doc.createTextNode("sg-bmsl-cdk-requisition-20190617144535.xml"));
		batchHeader.appendChild(filename);

	}

	private void createXMLDataDetails(Document doc, Element objectCollectionElement, List<String[]> records) {
		

		String firstRecord[] = null;
		for (String reqData[] : records) {
			firstRecord = reqData;
			break;
		}

		if (firstRecord == null) return ;

		
		Element object = doc.createElement("object");
		objectCollectionElement.appendChild(object);

		Element requisitionHeaderElement = doc.createElement("requisition-header");
		object.appendChild(requisitionHeaderElement);

		Element status = doc.createElement("status");
		status.appendChild(doc.createTextNode(firstRecord[0]));
		requisitionHeaderElement.appendChild(status);

	/*	Element justification = doc.createElement("justification");
		justification.appendChild(doc.createTextNode(firstRecord[1]));
		requisitionHeaderElement.appendChild(justification);
*/
		Element customFields = doc.createElement("custom-fields");
		requisitionHeaderElement.appendChild(customFields);

		Element cdkOrderNumber = doc.createElement("cdk-order-number");
		cdkOrderNumber.appendChild(doc.createTextNode("CDK-"+(firstRecord[4]).trim()));
		customFields.appendChild(cdkOrderNumber);

		Element cdkPosCompany = doc.createElement("cdk-pos-company");
		cdkPosCompany.appendChild(doc.createTextNode(firstRecord[5]));
		customFields.appendChild(cdkPosCompany);

		Element subletOrder = doc.createElement("sublet_order");
		subletOrder.appendChild(doc.createTextNode(firstRecord[6]));
		customFields.appendChild(subletOrder);

		Element currency = doc.createElement("currency");
		requisitionHeaderElement.appendChild(currency);
		Element code = doc.createElement("code");
		code.appendChild(doc.createTextNode(firstRecord[11]));
		currency.appendChild(code);

		Element requestedBy = doc.createElement("requested-by");
		requisitionHeaderElement.appendChild(requestedBy);
		Element login = doc.createElement("login");
		login.appendChild(doc.createTextNode(firstRecord[2]));
		requestedBy.appendChild(login);
	
		
		Element shiptoAddress = doc.createElement("ship-to-address");
		requisitionHeaderElement.appendChild(shiptoAddress);
		Element name = doc.createElement("id");
		name.appendChild(doc.createTextNode(firstRecord[3]));
		shiptoAddress.appendChild(name);

		Element createdBy = doc.createElement("created-by");
		requisitionHeaderElement.appendChild(createdBy);
		Element loginCreated = doc.createElement("login");
		loginCreated.appendChild(doc.createTextNode(firstRecord[1]));
		createdBy.appendChild(loginCreated);

		Element requisitionlines = doc.createElement("requisition-lines");
		requisitionHeaderElement.appendChild(requisitionlines);

		for (String reqData[] : records) {

			Element requisitionline = doc.createElement("requisition-line");
			requisitionlines.appendChild(requisitionline);

			Element description = doc.createElement("description");
			description.appendChild(doc.createTextNode(reqData[8]));
			requisitionline.appendChild(description);

			Element lineNum = doc.createElement("line-num");
			lineNum.appendChild(doc.createTextNode(reqData[7].trim()));
			requisitionline.appendChild(lineNum);

			Element unitPrice = doc.createElement("unit-price");
			unitPrice.appendChild(doc.createTextNode(reqData[9]));
			requisitionline.appendChild(unitPrice);

			Element customFieldsLineItem = doc.createElement("custom-fields");
			requisitionline.appendChild(customFieldsLineItem);

					
			Element LinecdkOrderNumber = doc.createElement("cdk-order-number");
			LinecdkOrderNumber.appendChild(doc.createTextNode("CDK-"+(reqData[4]).trim()));
			customFieldsLineItem.appendChild(LinecdkOrderNumber);
			
			
			Element cdkOperator = doc.createElement("cdk-operator");
			cdkOperator.appendChild(doc.createTextNode(reqData[2]));
			customFieldsLineItem.appendChild(cdkOperator);
			
			Element carLicensePlate = doc.createElement("car-license-plate");
			carLicensePlate.appendChild(doc.createTextNode(reqData[17]));
			customFieldsLineItem.appendChild(carLicensePlate);

			Element cdkVINNumber = doc.createElement("vehicle-identification-number-vin");
			cdkVINNumber.appendChild(doc.createTextNode(reqData[18]));
			customFieldsLineItem.appendChild(cdkVINNumber);
			
			Element cdkWipNumber = doc.createElement("cdk-wip-number");
			cdkWipNumber.appendChild(doc.createTextNode(reqData[14].trim()));
			customFieldsLineItem.appendChild(cdkWipNumber);

			Element cdkWipLine = doc.createElement("cdk-wip-line");
			cdkWipLine.appendChild(doc.createTextNode(reqData[15].trim()));
			customFieldsLineItem.appendChild(cdkWipLine);

			Element cdkRtCode = doc.createElement("cdk-rts-options-code");
			cdkRtCode.appendChild(doc.createTextNode(reqData[16]));
			customFieldsLineItem.appendChild(cdkRtCode);
			
			Element cdkModel = doc.createElement("model");
			cdkModel.appendChild(doc.createTextNode(reqData[19]));
			customFieldsLineItem.appendChild(cdkModel);

			Element cdkStockNumber = doc.createElement("cdk-stock-number");
			cdkStockNumber.appendChild(doc.createTextNode(reqData[20]));
			customFieldsLineItem.appendChild(cdkStockNumber);	
			
			Element cdkChangeBlocker = doc.createElement("cdk_change_blocker");
			cdkChangeBlocker.appendChild(doc.createTextNode(reqData[21]));
			customFieldsLineItem.appendChild(cdkChangeBlocker);				

			Element account = doc.createElement("account");
			requisitionline.appendChild(account);
			Element codeLineItem2 = doc.createElement("code");
			codeLineItem2.appendChild(doc.createTextNode(reqData[13]));
			account.appendChild(codeLineItem2);

			Element accountType = doc.createElement("account-type");
			account.appendChild(accountType);

			Element nameLineItem2 = doc.createElement("name");
			nameLineItem2.appendChild(doc.createTextNode(reqData[12]));
			accountType.appendChild(nameLineItem2);

			Element currencyLineItem = doc.createElement("currency");
			requisitionline.appendChild(currencyLineItem);
			Element codeLineItem = doc.createElement("code");
			codeLineItem.appendChild(doc.createTextNode(reqData[11]));
			currencyLineItem.appendChild(codeLineItem);

			Element supplier = doc.createElement("supplier");
			requisitionline.appendChild(supplier);
			Element nameLineItem = doc.createElement("name");
			nameLineItem.appendChild(doc.createTextNode(reqData[10]));
			supplier.appendChild(nameLineItem);

	/*		Element createdBy = doc.createElement("created-by");
			requisitionline.appendChild(createdBy);
			Element loginLineItem = doc.createElement("login");
			loginLineItem.appendChild(doc.createTextNode(reqData[1]));
			createdBy.appendChild(loginLineItem);
*/
		}

	
	}


}