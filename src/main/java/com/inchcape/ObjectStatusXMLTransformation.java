package com.inchcape;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ObjectStatusXMLTransformation {

	public static String NEW_LINE_SEPARATOR = System.getProperty("line.separator");

	private final static String BUSINESS_ID = "business-id";
	private final static String OBJECT_ID = "object-id";
	private final static String CDK_POS_COMPANY = "cdk-pos-company";
	private final static String CDK_WIP_NUMBER = "cdk-wip-number";
	private final static String CDK_WIP_LINE = "cdk-wip-line";
	private final static String STATUS = "status";
	private final static String INTEGRATION_MESSAGE = "integration-message";
	private final static String OBJECT_STATUS = "object-status";
	private final static String OBJECT_LINE = "object-line";

	private final static String INTEGRATION_RUN_ID = "integration-run-id";
	private final static String OBJECT_LINE_ID = "id";
	private final static String OBJECT_TYPE = "object-type";
	
	
	private final static String RESPONSE = "response";
	private final static String REQUISITION_LINE = "requisition-line";
	private final static String DESC = "description";
	
	private final static String ORDER_LINE = "order-line";
	
	private final static String BATCH_HEADER = "batch-header";
	
	private final static String ORDER_LINES_CHANGE = "order-line-change";
	 
	
	public static void main(String args[]) throws Exception {
		String filename = "src/test/resources/po-change-status ERROR and success.xml";
				String contents = new String(Files.readAllBytes(Paths.get(filename)));


		String resp = new ObjectStatusXMLTransformation().transformToCDKObjectStatusXMLDataStr(contents);
		System.out.println(resp);
	}

	// 1) will check if there are getting Response elements first for po-change-status & po-line-delete-status, and it is custom fields are from order_line.
	//   if it is error for for po-change-status, and it is custom fields are from order-line-change
	// 2) and then will check if there are getting object-status elements for po-status & grn_status, , and it is custom fields are from object_line
	public String transformToCDKObjectStatusXMLDataStr(String objectStatusRespXML) throws Exception {
       
		List<ObjectStatusreturnPayload> reqPayload = new ArrayList();
	
		getReqPayloadData (objectStatusRespXML,RESPONSE , reqPayload);
		getReqPayloadData (objectStatusRespXML,OBJECT_STATUS, reqPayload);
		CDKObjectStatusXMLGenerator generator = new CDKObjectStatusXMLGenerator();
		String respXML = generator.transformReqToResponse(reqPayload);
		return respXML;
	}
	

	
	private void getReqPayloadData(String requestXML, String rootTagName, List<ObjectStatusreturnPayload> reqPayloadList ) throws Exception {

		//List<ObjectStatusreturnPayload> reqPayloadList = new ArrayList();
		if (requestXML == null){
			return ;
		}
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		InputStream targetXmlStream = new ByteArrayInputStream(requestXML.getBytes());
		Document document = builder.parse(targetXmlStream);
		document.getDocumentElement().normalize();
		
		NodeList batchHeaderList = document.getElementsByTagName(this.BATCH_HEADER);
		
		// get object type from header
		String objectType="";
	
		for (int temp = 0; temp < batchHeaderList.getLength(); temp++) {
			Node node = batchHeaderList.item(temp);
			Element element = (Element) node;
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				NodeList nodeTmp = element.getElementsByTagName(this.OBJECT_TYPE);
				for (int i = 0, j = nodeTmp.getLength(); i < j; i++) {
					objectType = nodeTmp.item(0).getTextContent();
				}
			}			
		}
		
		NodeList nList = document.getElementsByTagName(rootTagName);
		
		for (int temp = 0; temp < nList.getLength(); temp++) {
			ObjectStatusreturnPayload ObjectStatusreturnPayload = new ObjectStatusreturnPayload();
			Node node = nList.item(temp);
			
			if (node.getNodeType() == Node.ELEMENT_NODE) {

				Element element = (Element) node;
				NodeList nodeTmp = element.getElementsByTagName(INTEGRATION_RUN_ID);
				for (int i = 0, j = nodeTmp.getLength(); i < j; i++) {

					ObjectStatusreturnPayload.setIntegrationId(nodeTmp.item(0).getTextContent());
				}
				
				nodeTmp = element.getElementsByTagName(BUSINESS_ID);
				for (int i = 0, j = nodeTmp.getLength(); i < j; i++) {
					
					ObjectStatusreturnPayload.setBusinessId(nodeTmp.item(0).getTextContent());
					
				}

				nodeTmp = element.getElementsByTagName(CDK_POS_COMPANY);
				for (int i = 0, j = nodeTmp.getLength(); i < j; i++) {
					ObjectStatusreturnPayload.setPosCompany(nodeTmp.item(0).getTextContent());
					// for error response, no business id, which can be got from pos company
					if (ObjectStatusreturnPayload.getBusinessId().equals("")){
						ObjectStatusreturnPayload.setBusinessId(ObjectStatusreturnPayload.getPosCompany());
						
					}
				}

				nodeTmp = element.getElementsByTagName(STATUS);
				for (int i = 0, j = nodeTmp.getLength(); i < j; i++) {
					ObjectStatusreturnPayload.setStatus(nodeTmp.item(0).getTextContent());
				}
				
				String objectTypeTmp = objectType;
                
				if (ObjectStatusreturnPayload.getStatus().equalsIgnoreCase("error") ){
					objectTypeTmp = CDKObjectStatusXMLGenerator.OBJECT_TYPE_ERROR;
				}
				
				ObjectStatusreturnPayload.setObjectType(objectTypeTmp );

				nodeTmp = element.getElementsByTagName(OBJECT_ID);
				for (int i = 0, j = nodeTmp.getLength(); i < j; i++) {
					ObjectStatusreturnPayload.setObjectId(nodeTmp.item(0).getTextContent());
				}
                
				if (rootTagName.equalsIgnoreCase(RESPONSE)){
					nodeTmp = element.getElementsByTagName(DESC); 
				}else{				
				   nodeTmp = element.getElementsByTagName(INTEGRATION_MESSAGE);
				
				}
				for (int i = 0, j = nodeTmp.getLength(); i < j; i++) {

					ObjectStatusreturnPayload.setIntegraitonMsg(nodeTmp.item(0).getTextContent());
				}


                if (rootTagName.equalsIgnoreCase(RESPONSE)){
                	 
                	if ( ObjectStatusreturnPayload.getStatus().equalsIgnoreCase("error") &&
                			 ( objectType.equalsIgnoreCase(CDKObjectStatusXMLGenerator.OBJECT_TYPE_CHANGE)  ) ) {
                		nodeTmp = element.getElementsByTagName(ORDER_LINES_CHANGE);  
                	}else{
                		nodeTmp = element.getElementsByTagName(ORDER_LINE); 
                	}
                	
                }else{
				    nodeTmp = element.getElementsByTagName(OBJECT_LINE);
                }
                
				List<ObjectStatusLine> ObjectStatusLineList = new ArrayList();
				for (int i = 0, j = nodeTmp.getLength(); i < j; i++) {
					Element elementOjbectLine = (Element) nodeTmp.item(i);
					NodeList nodeTmpOjbectLine = elementOjbectLine.getElementsByTagName(CDK_WIP_NUMBER);

					ObjectStatusLine ObjectStatusLine = new ObjectStatusLine();
					int lineNo = i+1 ;
					ObjectStatusLine.setLineNo(lineNo+"");
					for (int ii = 0, jj = nodeTmpOjbectLine.getLength(); ii < jj; ii++) {
						ObjectStatusLine.setWipNumber(nodeTmpOjbectLine.item(0).getTextContent());
					}

					nodeTmpOjbectLine = elementOjbectLine.getElementsByTagName(CDK_WIP_LINE);
					for (int ii = 0, jj = nodeTmpOjbectLine.getLength(); ii < jj; ii++) {
						ObjectStatusLine.setWipLine(nodeTmpOjbectLine.item(0).getTextContent());
					}

					nodeTmpOjbectLine = elementOjbectLine.getElementsByTagName(OBJECT_LINE_ID);
					for (int ii = 0, jj = nodeTmpOjbectLine.getLength(); ii < jj; ii++) {
						ObjectStatusLine.setId(nodeTmpOjbectLine.item(0).getTextContent());
					}
					ObjectStatusLineList.add(ObjectStatusLine);
				}
				ObjectStatusreturnPayload.setObjectStatusLine(ObjectStatusLineList);

				reqPayloadList.add(ObjectStatusreturnPayload);
			}
		}
		
	}

}
