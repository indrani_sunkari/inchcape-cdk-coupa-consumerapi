package com.inchcape;

import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class CoupaXMLGenerator {
	
	private final static String OBJECT_SYNC_REQUEST = "object-sync-request";
	private final static String OBJECT_TYPE = "object-type";
	private final static String SYNC_TYPE = "sync-type";
	private final static String BUSINESS_ENTITY_ID = "business-entity-id";
//	private final static String COUPA_CODE = "coupa-code";
	private final static String INTEGRATION_RUN_ID = "integration-run-id";
	private final static String OBJECT_RETRIEVED_COUNT = "object-retrieved-count";
	private final static String REQUEST_TIME_STAMP = "requestTimeStamp";
	
	private final static String OBJECT_COLLECTION = "object-collection";	
	private final static String OBJECT = "object";
	private final static String OBJECT_LINES = "object-lines";
	private final static String OBJECT_LINE = "object-line";
	private final static String ID = "id";
	private final static String INTEGRATION_STATUS = "integration-status";
	private final static String INTEGRATION_MESSAGE = "integration-message";
	
	private final static String CUSTOM_FIELDS = "custom-fields";
//	private final static String SUBLET_ORDER = "sublet-order";
//	private final static String CDK_ORDER_NUMBER = "cdk-order-number";
//	private final static String CDK_POS_COMPANY = "cdk-pos-company";
    
	private final static String CDK_WIP_NUMBER = "cdk-wip-number";
	private final static String CDK_WIP_LINE = "cdk-wip-line";
	
	private final static String OBJECT_TYPE_DEFALUT_VALUE = "po-status";
	private final static String SYNC_TYPE_DEFAULT_VALUE = "end";
	
	
	// this methods is to generate Coupa xml data
	public String generateCoupaObjSyncReq(List<SubletBatch> subletBatchList) throws Exception {
		String coupaObjSyncReq = null;
		 
		if (subletBatchList == null || subletBatchList.size() == 0 ) {
			return null;
		}
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();	
		Document doc = docBuilder.newDocument();
		
		Element rootElement = doc.createElement(OBJECT_SYNC_REQUEST);
		doc.appendChild(rootElement);
		
		setCoupaHeader(subletBatchList,rootElement, doc); 
		setObjectCollection(subletBatchList,rootElement, doc);
		
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC, "yes");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
		//transformer.setOutputProperty("omit-xml-declaration", "yes");
		DOMSource source = new DOMSource(doc);

		StreamResult result = new StreamResult(new StringWriter());

		transformer.transform(source, result);
		coupaObjSyncReq = result.getWriter().toString();
		return coupaObjSyncReq;
	}
	
	private void setCoupaHeader (List<SubletBatch> subletBatchList,Element rootElement, Document doc){
		SubletBatch subletBatch = (SubletBatch) subletBatchList.get(0);
		
		Element objectTypeElement = doc.createElement(OBJECT_TYPE);
		objectTypeElement.appendChild(doc.createTextNode(OBJECT_TYPE_DEFALUT_VALUE));
		rootElement.appendChild(objectTypeElement);
		
		Element syncTypeElement = doc.createElement(SYNC_TYPE);
		syncTypeElement.appendChild(doc.createTextNode(SYNC_TYPE_DEFAULT_VALUE));
		rootElement.appendChild(syncTypeElement);
		
		Element businessIdElement = doc.createElement(BUSINESS_ENTITY_ID);
		businessIdElement.appendChild(doc.createTextNode(subletBatch.getId()));
		rootElement.appendChild(businessIdElement);
		
		Element integrationRunIdElement = doc.createElement(INTEGRATION_RUN_ID);
		integrationRunIdElement.appendChild(doc.createTextNode(subletBatch.getIntegrationId()));
		rootElement.appendChild(integrationRunIdElement);
		
		Element objectRetrivedCountElement = doc.createElement(OBJECT_RETRIEVED_COUNT);
		objectRetrivedCountElement.appendChild(doc.createTextNode(subletBatchList.size() + ""));
		rootElement.appendChild(objectRetrivedCountElement);
		
		Element reqTimeStampElement = doc.createElement(REQUEST_TIME_STAMP);
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(new Date());
		reqTimeStampElement.appendChild(doc.createTextNode(timeStamp));
		rootElement.appendChild(reqTimeStampElement);
	}
	
	private void setObjectCollection(List<SubletBatch> subletBatchList, Element rootElement, Document doc) throws Exception {	
		Element objectCollectionElement = doc.createElement(OBJECT_COLLECTION);
		rootElement.appendChild(objectCollectionElement);
		
		for (SubletBatch subletBatch : subletBatchList) {
			Element objectElement = doc.createElement(OBJECT);
			objectCollectionElement.appendChild(objectElement);
			int subletLineCount = 0;
			
			Element objectLinesElement = doc.createElement(OBJECT_LINES);
			
			List<SubletLine> subletLineList = subletBatch.getSubletLine();
			String objectIntegrationMsg = "";
			
			for (SubletLine subletLine : subletLineList) {
				if ( subletLineCount== 0){
					Element objectIdElement = doc.createElement(ID);
					objectIdElement.appendChild(doc.createTextNode(subletLine.getCoupaPoNoFirstSeg()));
					objectElement.appendChild(objectIdElement);
					
					Element objectIntegrationStatusElement = doc.createElement(INTEGRATION_STATUS);
					objectIntegrationStatusElement.appendChild(doc.createTextNode(subletLine.getIntegrationStatus()));
					objectElement.appendChild(objectIntegrationStatusElement);
					
				}
				
				subletLineCount ++;
				
				objectIntegrationMsg = objectIntegrationMsg + subletLine.getIntegrationMsg() + ";";
				
				Element objectLineElement = doc.createElement(OBJECT_LINE);
				objectLinesElement.appendChild(objectLineElement);
				
				Element objectLineIdElement = doc.createElement(ID);
				objectLineIdElement.appendChild(doc.createTextNode(subletLine.getCoupaPoNoThirdSeg()));
				objectLineElement.appendChild(objectLineIdElement);
				
				Element objectLineIntegrationStatusElement = doc.createElement(INTEGRATION_STATUS);
				objectLineIntegrationStatusElement.appendChild(doc.createTextNode(subletLine.getIntegrationStatus() ));
				objectLineElement.appendChild(objectLineIntegrationStatusElement);
				
				Element objectLineIntegrationMsgElement = doc.createElement(INTEGRATION_MESSAGE);
				objectLineIntegrationMsgElement.appendChild(doc.createTextNode(subletLine.getIntegrationMsg() ));
				objectLineElement.appendChild(objectLineIntegrationMsgElement);
				
				Element objectLineCustomFieldsElement = doc.createElement(CUSTOM_FIELDS);
				objectLineElement.appendChild(objectLineCustomFieldsElement);
				
				Element objectLineCustomFieldsWipNoElement = doc.createElement(CDK_WIP_NUMBER);
				objectLineCustomFieldsWipNoElement.appendChild(doc.createTextNode(subletLine.getWipNo() ));
				objectLineCustomFieldsElement.appendChild(objectLineCustomFieldsWipNoElement);
				
				Element objectLineCustomFieldsWipLineElement = doc.createElement(CDK_WIP_LINE);
				objectLineCustomFieldsWipLineElement.appendChild(doc.createTextNode(subletLine.getWipLine() ));
				objectLineCustomFieldsElement.appendChild(objectLineCustomFieldsWipLineElement);
			}
			// remove last semicolon
			objectIntegrationMsg = objectIntegrationMsg.substring(0,objectIntegrationMsg.length()-2);
			Element objectIntegrationMsgElement = doc.createElement(INTEGRATION_MESSAGE);
			objectIntegrationMsgElement.appendChild(doc.createTextNode(objectIntegrationMsg ));
			objectElement.appendChild(objectIntegrationMsgElement);
			
			objectElement.appendChild(objectLinesElement);
			
		}
		
	}
}
