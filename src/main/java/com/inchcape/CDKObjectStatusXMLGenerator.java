package com.inchcape;

import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;


//Timezone to be updated.
//import org.python.icu.util.TimeZone;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

class CDKObjectStatusXMLGenerator {
	private static final String SUBLET_BATCH = "SubletBatch";
	private static final String ID = "id";
	private static final String SUBLET_LINE = "subletLine";
	private static final String WIP_NO = "WIPNo";
	private static final String WIP_LINE = "WIPLine";
	private static final String ORDER_STATUS = "OrderStatus";
	private static final String INTEGRATION_MSG = "IntegrationMessage";
	private static final String STATUS_ERROR = "error";
	private static final String POS_COMPANY = "POSCompany";

	private static final String COUPA_PO_NO = "CoupaPONO";
	private static final String GRN = "GRN";
	private static final String OBJECT_TYPE_PO = "po-status";
	public static final String OBJECT_TYPE_ERROR = "error";
	public static final String OBJECT_TYPE_CHANGE = "po-change-status";
	private static final String OBJECT_TYPE_DELETE = "po-line-delete-status";
	private static final String OBJECT_TYPE_GRN = "grn-status";
	private static final String INTEGRATION_ID = "IntegrationId";
	
	private static final String STATUS_ERROR_FLAG = "E"; // error response flag
	private static final String STATUS_GRN_FLAG = "6";  // grn-status
	private static final String STATUS_PO_FLAG = "4"; // objectType = po-status flag
	private static final String STATUS_CHANGE_FLAG = "5"; //po-change-status
	private static final String STATUS_DELETE_FLAG = "7"; //po-line-delete-status 

	
	// Assume: 
	// 1) it will have only one SubletBatch root element for returning xml	
	// 2) all of ObjectStatusreturnPayload objects will have the same integration id and ID
	public String transformReqToResponse(List<ObjectStatusreturnPayload> responsePayloadList) throws Exception {
		String xmlResult = null;
		if (responsePayloadList == null || responsePayloadList.size() == 0) {
			return null;
		}
		ObjectStatusreturnPayload objectStatusReturnPayload = (ObjectStatusreturnPayload) responsePayloadList.get(0);
		if (objectStatusReturnPayload == null) {
			return null;
		}
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

		// root elements
		Document doc = docBuilder.newDocument();
		Element rootElement = doc.createElement(SUBLET_BATCH);
		doc.appendChild(rootElement);

		Element idElement = doc.createElement(ID);
		String idStr = objectStatusReturnPayload.getBusinessId();
		if (idStr != null) {
			String splitIdStr[] = idStr.split("-");
			int idLen = splitIdStr.length;

			if (idLen >= 2) { // e.g. SG-BMSL-XXX-XXX, will get SG-BMSL only
				idStr = splitIdStr[0] + "-" + splitIdStr[1];

			} else {
				// do nothing, set whole id string;
			}

		}

		idElement.appendChild(doc.createTextNode(idStr));
		rootElement.appendChild(idElement);		
         
		String integrationId = objectStatusReturnPayload.getIntegrationId();
		
		if (integrationId != null ) {
			Element ingetrationIdElement = doc.createElement(INTEGRATION_ID);
			ingetrationIdElement.appendChild(doc.createTextNode(integrationId));
			rootElement.appendChild(ingetrationIdElement);
		}

		transformReqToResponse(responsePayloadList, rootElement, doc,idStr);
		
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC, "yes");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
		//transformer.setOutputProperty("omit-xml-declaration", "yes");
		DOMSource source = new DOMSource(doc);

		StreamResult result = new StreamResult(new StringWriter());

		transformer.transform(source, result);
		xmlResult = result.getWriter().toString();

		return xmlResult;
	}

	private void transformReqToResponse(List<ObjectStatusreturnPayload> responsePayloadList, Element rootElement,
			Document doc,String businessIdStr) throws Exception {
		for (ObjectStatusreturnPayload objectStatusReturnPayload : responsePayloadList) {
			
			String statusTmp = objectStatusReturnPayload.getObjectType();
			
			if (statusTmp.equalsIgnoreCase(OBJECT_TYPE_ERROR) ){
				statusTmp = STATUS_ERROR_FLAG;
			}else if (statusTmp.equalsIgnoreCase(OBJECT_TYPE_PO)) {
				statusTmp = STATUS_PO_FLAG;
			}else if (statusTmp.equalsIgnoreCase(this.OBJECT_TYPE_CHANGE)) {
				statusTmp = STATUS_CHANGE_FLAG;
			}else if (statusTmp.equalsIgnoreCase(this.OBJECT_TYPE_GRN)) {
				statusTmp = STATUS_GRN_FLAG;
			}else if (statusTmp.equalsIgnoreCase(this.OBJECT_TYPE_DELETE)) {
				statusTmp = STATUS_DELETE_FLAG;
			}else{
				statusTmp = "";
			}
			
			String posCompany = objectStatusReturnPayload.getPosCompany();
			if ( statusTmp.equals(STATUS_ERROR_FLAG)){
				String splitStr1[] = posCompany.split("-");
				int len1 = splitStr1.length;

				if (len1 >= 3) { // e.g. SG-BMSL-52, will get 52 only
					posCompany = splitStr1[2];
				}		
			} else{
				if (posCompany != null) {
					String splitStr[] = posCompany.split("-");
					int len = splitStr.length;
	
					if (len == 3) { // e.g. SG-BMSL-52, will get 52 only
						posCompany = splitStr[2];
	
					} else {
						posCompany = "";
					}
				}
			}
			
		
			List<ObjectStatusLine> ObjectStatusLineList = objectStatusReturnPayload.getObjectStatusLine();
			for (ObjectStatusLine ObjectStatusLine : ObjectStatusLineList) {
				Element subletLineElement = doc.createElement(SUBLET_LINE);
				rootElement.appendChild(subletLineElement);

				Element posCompanyElement = doc.createElement(POS_COMPANY);
				if (posCompany != "") {
					posCompanyElement.appendChild(doc.createTextNode(posCompany));
					subletLineElement.appendChild(posCompanyElement);
				}

				Element wipNoElement = doc.createElement(WIP_NO);
				wipNoElement.appendChild(doc.createTextNode(ObjectStatusLine.getWipNumber()));
				subletLineElement.appendChild(wipNoElement);

				Element wipLineElement = doc.createElement(WIP_LINE);
				String wipLine = ObjectStatusLine.getWipLine();
				if (wipLine != null) {
					String splitStr[] = wipLine.split("-");
					if (splitStr.length == 3) {//e.g. WIP-LINE-10, will get 10 only
						wipLine = splitStr[2];
					} else {
						// do nothing, set whole wipLine string;
					}
				}
				wipLineElement.appendChild(doc.createTextNode(wipLine));
				subletLineElement.appendChild(wipLineElement);

				Element orderStatusElement = doc.createElement(ORDER_STATUS);				
				orderStatusElement.appendChild(doc.createTextNode(statusTmp));
				subletLineElement.appendChild(orderStatusElement);

				//String timeStamp = new SimpleDateFormat("dd/MM/yyyy HH:mm").format(new Date());
				
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
				if (businessIdStr!= null){
					if (businessIdStr.length() >=2){
						if (businessIdStr.substring(0, 2).equalsIgnoreCase("SG") ){
							sdf.setTimeZone(TimeZone.getTimeZone("Asia/Singapore"));
						}else  if (businessIdStr.substring(0, 2).equalsIgnoreCase("HK") ){
							sdf.setTimeZone(TimeZone.getTimeZone("Hongkong"));
						}
					}
				}
				String timeStamp = sdf.format(new Date());
		

				Element ingegrationMsgElement = doc.createElement(INTEGRATION_MSG);
				ingegrationMsgElement.appendChild(
						doc.createTextNode(objectStatusReturnPayload.getIntegraitonMsg() + " @" + timeStamp));
				subletLineElement.appendChild(ingegrationMsgElement);
				if (statusTmp.equals(STATUS_PO_FLAG)) { 
					Element coupaPONO = doc.createElement(COUPA_PO_NO);
					String coupaPoNoStr = objectStatusReturnPayload.getObjectId() + "-" + ObjectStatusLine.getLineNo()
							+ "-" + ObjectStatusLine.getId();
					coupaPONO.appendChild(doc.createTextNode(coupaPoNoStr));
					subletLineElement.appendChild(coupaPONO);

				} else if (statusTmp.equals(STATUS_GRN_FLAG)) {
					Element grnElement = doc.createElement(GRN);
					grnElement.appendChild(doc.createTextNode(objectStatusReturnPayload.getObjectId()));
					subletLineElement.appendChild(grnElement);

				}
			} // end for (ObjectStatusLine ObjectStatusLine : ObjectStatusLineList) {
		} // end for (ObjectStatusreturnPayload objectStatusReturnPayload : responsePayloadList) {

	}


}
