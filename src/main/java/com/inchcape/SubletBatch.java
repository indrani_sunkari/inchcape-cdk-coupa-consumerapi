package com.inchcape;

import java.util.List;

public class SubletBatch {
	private String integrationId = "";
	private String id = "";
	private List<SubletLine> SubletLine ;
	 
	
	public String getIntegrationId() {
		return integrationId;
	}
	public void setIntegrationId(String integrationId) {
		this.integrationId = integrationId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public List<SubletLine> getSubletLine() {
		return SubletLine;
	}
	public void setSubletLine(List<SubletLine> subletLine) {
		SubletLine = subletLine;
	}
	 
}
