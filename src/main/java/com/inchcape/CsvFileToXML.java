package com.inchcape;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class CsvFileToXML { 

	public static String NEW_LINE_SEPARATOR = System.getProperty("line.separator");

	private String csvData;
	private String delimiter;
	private String tmp;

	public static void main(String args[]) throws Exception {
		CsvFileToXML csvFileToXML = new CsvFileToXML();

		String newLine = NEW_LINE_SEPARATOR;
		
		
		String str = "Status,Created By,Requested By,Ship to Id,CDK Order Number,CDK PoS Company,Sublet,Line Number,Description,Price,Supplier Name,Currency Code,Chart of Accout Name,Account Code,CDK WIP number,CDK WIP LINE,CDK RTS Code,Car Licence Plate,Vehicle Identification Number (VIN),Model,Stock Number,CDK Change Blocker,"
				+ newLine
				+"pending_approval,neil.burns2@inchcape.com,steve.fossett@inchcape.com,850, 2848 ,SG-BMSL-JA,TRUE, 23701501 ,Sublet order - Repleace Windscreen, 174 ,SG-BMSL-A0000001,GBP,SG Borneo Motors (Singapore) Pte. Ltd (2200),05-51-957501, 23701 , 1 ,SUB,YEB546,SAJAC63H3FMK15164,Jaguar F-type Coupe 5.0 Superc, ,No,"
				+newLine
				+"pending_approval,neil.burns2@inchcape.com,steve.fossett@inchcape.com,850, 2848 ,SG-BMSL-JA,TRUE, 23701502 ,Replace quater glass seal, 15 ,SG-BMSL-A0000001,GBP,SG Borneo Motors (Singapore) Pte. Ltd (2200),05-51-957501, 23701 , 2 ,SUB,YEB546,SAJAC63H3FMK15164,Jaguar F-type Coupe 5.0 Superc, ,No,"
				+ newLine;
		
	
		
		String data = csvFileToXML.convertCSVFileFormatToXML(str, ",");
		 
	     System.out.println(data);
 
	}

	public void setCsvData(String csvData) {
		this.csvData = csvData;
	}

	public String getCsvData() {

		return this.csvData;
	}

	public String convertCSVFileFormatToXML() throws Exception {
		return convertCSVFileFormatToXML(this.csvData, ",");
	}
	
	
	public String convertCSVFileFormatToXML(String csvData) throws Exception {
		return convertCSVFileFormatToXML(csvData, ",");
	}

	private String convertCSVFileFormatToXML(String csvData, String delimiter) throws Exception {

		if (csvData == null) {
			throw new Exception("csvData is null");
		}

		if (delimiter == null) {
			throw new Exception("delimiter is null");
		}

		String lines[] = csvData.split(NEW_LINE_SEPARATOR);

		int fieldCount = 0;
		String[] csvFields = null;
		StringTokenizer stringTokenizer = null;

		List<String[]> data = new ArrayList();

		String curLine = "";
		int len = lines.length;

		for (int j = 1; j < len; j++) {
			curLine = lines[j];

			System.out.println(curLine);
			stringTokenizer = new StringTokenizer(curLine, delimiter);
			fieldCount = stringTokenizer.countTokens();
			csvFields = new String[fieldCount];
			// System.out.println("fieldCount:" + fieldCount);
			if (fieldCount > 0) {
				int i = 0;
				String str = "";
				while (stringTokenizer.hasMoreElements()) {
					String tmp = String.valueOf(stringTokenizer.nextElement());
				
					str = tmp.replace("\"", "");	
					csvFields[i++] = str;
					str = "";
				}
			}
			data.add(csvFields);
		}
		// System.out.println (data.size());

		XMLGenerator xml = new XMLGenerator();
		return xml.createXML(data);

	}

}