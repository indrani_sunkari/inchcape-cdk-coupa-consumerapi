package com.inchcape;
import java.util.List;

import com.inchcape.ObjectStatusLine;

public class ObjectStatusreturnPayload {
	private String objectId = "";
	private String posCompany = "";	
	private String status = "";
	private String integraitonMsg = "";
	private List<ObjectStatusLine> ObjectStatusLine ;
	private String integrationId = "";
	private String coupaPoNo = "";
	private String objectType = "";
	private String businessId = "";

	public String getBusinessId() {
		return businessId;
	}

	public void setBusinessId(String businessId) {
		this.businessId = businessId;
	}

	public String getPosCompany() {
		return posCompany;
	}

	public void setPosCompany(String posCompany) {
		this.posCompany = posCompany;
	}
	

	public List<ObjectStatusLine> getObjectStatusLine() {
		return ObjectStatusLine;
	}

	public void setObjectStatusLine(List<ObjectStatusLine> ObjectStatusLine) {
		this.ObjectStatusLine = ObjectStatusLine;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIntegraitonMsg() {
		return integraitonMsg;
	}

	public void setIntegraitonMsg(String integraitonMsg) {
		this.integraitonMsg = integraitonMsg;
	}

	public String getIntegrationId() {
		return integrationId;
	}

	public void setIntegrationId(String integrationId) {
		this.integrationId = integrationId;
	}

	public String getCoupaPoNo() {
		return coupaPoNo;
	}

	public void setCoupaPoNo(String coupaPoNo) {
		this.coupaPoNo = coupaPoNo;
	}

	public String getObjectId() {
		return objectId;
	}

	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	public String getObjectType() {
		return objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}
}
