package com.inchcape;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class RequisitionXMLTransformation {

	public static String NEW_LINE_SEPARATOR = System.getProperty("line.separator");

	private final static String BUSINESS_ID = "business-id";
	private final static String CDK_POS_COMPANY = "cdk-pos-company";
	private final static String CDK_WIP_NUMBER = "cdk-wip-number";
	private final static String CDK_WIP_LINE = "cdk-wip-line";
	private final static String STATUS = "status";
	private final static String INTEGRATION_MESSAGE = "integration-message";
	private final static String RESPONSE = "response";
	private final static String OBJECT_LINE = "object-line";
	private final static String REQUISITION_LINE = "requisition-line";
	private final static String ERROR_DESC = "description";
	private final static String OBJECT_STATUS = "object-status";
	private final static String REQUISITION_HEADER = "requisition-header";

	public static void main(String args[]) throws Exception {
		 String filename = "src/test/resources/batch-requisition-response-success.xml";
		String contents = new String(Files.readAllBytes(Paths.get(filename)));

		String resp = new RequisitionXMLTransformation().transformToSubletBatchXMLDataStr(contents);
		System.out.println(resp);
	}

	public String transformToSubletBatchXMLDataStr(String requisitionReqXML) throws Exception {

		List<RequestPayload> reqPayload = getReqPayloadData(requisitionReqXML);

		SubletBatchXMLGenerator generator = new SubletBatchXMLGenerator();
		String response = generator.transformReqToResponse(reqPayload);
		return response;
	}

	private List<RequestPayload> getReqPayloadData(String requestXML) throws Exception {

		List<RequestPayload> reqPayloadList = new ArrayList();
		if (requestXML == null) {
			return reqPayloadList;
		}
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		InputStream targetXmlStream = new ByteArrayInputStream(requestXML.getBytes());
		Document document = builder.parse(targetXmlStream);
		document.getDocumentElement().normalize();
		NodeList nList = document.getElementsByTagName(RESPONSE);

		for (int temp = 0; temp < nList.getLength(); temp++) {

			Node node = nList.item(temp);

			if (node.getNodeType() == Node.ELEMENT_NODE) {

				Element elementResponse = (Element) node;

				NodeList nodeTmpStatus = elementResponse.getElementsByTagName(STATUS);
				String respStatus = "";
				for (int i = 0, j = nodeTmpStatus.getLength(); i < j; i++) {
					respStatus = nodeTmpStatus.item(0).getTextContent();
				}

				NodeList nodeTmp1 = null;
				if (respStatus.equalsIgnoreCase("error")) {
					nodeTmp1 = elementResponse.getElementsByTagName(REQUISITION_HEADER);
				} else {
					nodeTmp1 = elementResponse.getElementsByTagName(OBJECT_STATUS);
				}

				for (int it = 0, jt = nodeTmp1.getLength(); it < jt; it++) {
					RequestPayload requestPayload = new RequestPayload();
					Element element = (Element) nodeTmp1.item(it);

					NodeList nodeTmp = element.getElementsByTagName(CDK_POS_COMPANY);
					for (int i = 0, j = nodeTmp.getLength(); i < j; i++) {
						requestPayload.setPosCompany(nodeTmp.item(0).getTextContent());
					}

					for (int i = 0, j = nodeTmpStatus.getLength(); i < j; i++) {
						requestPayload.setStatus(respStatus);
					}

					nodeTmp = element.getElementsByTagName(BUSINESS_ID);
					for (int i = 0, j = nodeTmp.getLength(); i < j; i++) {
						requestPayload.setBusinessId(nodeTmp.item(0).getTextContent());
					}

					if (requestPayload.getStatus().equalsIgnoreCase("error")) {
						nodeTmp = elementResponse.getElementsByTagName(ERROR_DESC);
					} else {
						nodeTmp = element.getElementsByTagName(INTEGRATION_MESSAGE);
					}

					for (int i = 0, j = nodeTmp.getLength(); i < j; i++) {
						requestPayload.setIntegraitonMsg(nodeTmp.item(0).getTextContent());
					}

					if (requestPayload.getStatus().equalsIgnoreCase("error")) {
						nodeTmp = element.getElementsByTagName(REQUISITION_LINE);
					} else {
						nodeTmp = element.getElementsByTagName(OBJECT_LINE);
					}

					List<ObjectLine> objectLineList = new ArrayList();
					for (int i = 0, j = nodeTmp.getLength(); i < j; i++) {
						Element elementOjbectLine = (Element) nodeTmp.item(i);
						NodeList nodeTmpOjbectLine = elementOjbectLine.getElementsByTagName(CDK_WIP_NUMBER);

						ObjectLine objectLine = new ObjectLine();
						for (int ii = 0, jj = nodeTmpOjbectLine.getLength(); ii < jj; ii++) {
							objectLine.setWipNumber(nodeTmpOjbectLine.item(0).getTextContent());
						}

						nodeTmpOjbectLine = elementOjbectLine.getElementsByTagName(CDK_WIP_LINE);
						for (int ii = 0, jj = nodeTmpOjbectLine.getLength(); ii < jj; ii++) {
							objectLine.setWipLine(nodeTmpOjbectLine.item(0).getTextContent());
						}
						objectLineList.add(objectLine);
					}
					requestPayload.setObjectLine(objectLineList);
					reqPayloadList.add(requestPayload);
				} // end for (int it = 0, jt = nodeTmp1.getLength(); it < jt;
					// it++)
			} // end for (int temp = 0; temp < nList.getLength(); temp++)
		}
		return reqPayloadList;
	}

}
