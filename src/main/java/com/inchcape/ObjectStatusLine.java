package com.inchcape;

public class ObjectStatusLine {
	
	private String wipNumber = "";
	private String wipLine = "";
	private String id = "";
	private String lineNo = "";
	
	public String getWipNumber() {
		return wipNumber;
	}

	public void setWipNumber(String wipNumber) {
		this.wipNumber = wipNumber;
	}

	public String getWipLine() {
		return wipLine;
	}

	public void setWipLine(String wipLine) {
		this.wipLine = wipLine;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLineNo() {
		return lineNo;
	}

	public void setLineNo(String lineNo) {
		this.lineNo = lineNo;
	}

}
