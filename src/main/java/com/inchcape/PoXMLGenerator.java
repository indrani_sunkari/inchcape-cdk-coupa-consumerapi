package com.inchcape;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class PoXMLGenerator {
	
	private final static String BATCH_OBJECT = "batch-object";
	private final static String BATCH_HEADER = "batch-header";
	private final static String OBJECT_TYPE = "object-type";
	private final static String BATH_ID = "batch-id";
	private final static String BATH_SIZE = "batch-size";
	private final static String FILE_NAME = "filename";
	
	
	private final static String OBJECT_COLLECTION = "object-collection";
	private final static String OBJECT = "object";
	private final static String ORDER_HEADER_CHANGE = "order-header-change";
	private final static String ORDER_HEADER_ID = "order-header-id";
	
	private final static String CUSTOM_FIELDS= "custom-fields";
	private final static String ORDER_CHANGE_BLOKER = "cdk-change-blocker";
	
	private final static String ORDER_LINE_CHANGES = "order-line-changes";
	private final static String ORDER_LINE_CHANGE = "order-line-change";
	private final static String LINE_NUM = "line-num";
	private final static String DESC = "description";
	private final static String PRICE = "price";
	private final static String CDK_RTS_CODE= "cdk-rts-code";
	private final static String CAR_LICENSE_PLATE = "car-license-plate";
	private final static String VIP_VIN_NO = "vehicle-identification-number-vin";
	private final static String CDK_POS_COMPANY= "cdk-pos-company";
	private final static String CDK_WIP_NO = "cdk-wip-number";
	private final static String CDK_WIP_LINE = "cdk-wip-line";
	
	private final static String CURRENCY = "currency";
	private final static String CODE = "code";
		 

	public String createPoXML(List<String[]> data) throws Exception {
		String orderNumberTmp = "";
		for (String reqData[] : data) {
			orderNumberTmp = reqData[0];
			break;
		}

		List<String[]> dataTmp = new ArrayList <String[]> ();
		int batchSize = 0;

		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		Document doc = docBuilder.newDocument();
		Element batchObjectRootElement = doc.createElement(BATCH_OBJECT);
		doc.appendChild(batchObjectRootElement);
		
		Element objectCollectionElement = doc.createElement(OBJECT_COLLECTION);
		

		for (String reqData[] : data) {
			String orderNumber = reqData[0];
			if (!orderNumberTmp.equals(orderNumber)) {

				createXMLDataDetails(doc,objectCollectionElement, dataTmp);
				batchSize = batchSize + 1;
				
				orderNumberTmp = orderNumber;
				dataTmp = new ArrayList <String[]> ();
				dataTmp.add(reqData);
				continue;
			}
			dataTmp.add(reqData);
		}

		batchSize = batchSize + 1;
		createXMLDataDetails(doc,objectCollectionElement, dataTmp);;
		 
		createXMLHeaderData (doc,batchObjectRootElement,batchSize+"" );
		
		batchObjectRootElement.appendChild(objectCollectionElement);
		
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC, "yes");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
		DOMSource source = new DOMSource(doc);

		StreamResult result = new StreamResult(new StringWriter());

		transformer.transform(source, result);
		String 	xmlResult = result.getWriter().toString();
		return xmlResult;
	}

	 // hard code 3 fields for now, needs to be clean upa later
	 // 1. FILE_NAME 2. BATH_ID 3. OBJECT_TYPE
	private void createXMLHeaderData(Document doc, Element batchObjectRootElement, String batchSizeStr) throws Exception {
		Element batchHeader = doc.createElement(BATCH_HEADER);
		batchObjectRootElement.appendChild(batchHeader);

		Element objectType = doc.createElement(OBJECT_TYPE);
		objectType.appendChild(doc.createTextNode("po-change")); // Hard code for now
		batchHeader.appendChild(objectType);
		
		Element batchId = doc.createElement(BATH_ID);
		// put 123456 for now, need to be changed later when got clear mapping
		batchId.appendChild(doc.createTextNode("123456")); // Hard code for now
		batchHeader.appendChild(batchId);

		Element batchSize = doc.createElement(BATH_SIZE);
		batchSize.appendChild(doc.createTextNode(batchSizeStr));
		batchHeader.appendChild(batchSize);

		Element filename = doc.createElement(FILE_NAME);
		// put hk-cml-cdk-po-change-20200109073113.xml for now, need to be
		// changed later when got clear mapping
		filename.appendChild(doc.createTextNode("hk-cml-cdk-po-change-20200109073113.xml"));
		batchHeader.appendChild(filename);

	}

	private void createXMLDataDetails(Document doc, Element objectCollectionElement, List<String[]> records) {
		

		String firstRecord[] = null;
		for (String reqData[] : records) {
			firstRecord = reqData;
			break;
		}

		if (firstRecord == null) return ;

		
		Element object = doc.createElement(OBJECT);
		objectCollectionElement.appendChild(object);

		Element orderHeaderElement = doc.createElement(ORDER_HEADER_CHANGE);
		object.appendChild(orderHeaderElement);

		Element headerId = doc.createElement(ORDER_HEADER_ID);
		headerId.appendChild(doc.createTextNode(firstRecord[0].trim()));
		orderHeaderElement.appendChild(headerId);

		Element customFieldsElement = doc.createElement(CUSTOM_FIELDS);
		orderHeaderElement.appendChild(customFieldsElement);

		Element changeBlock = doc.createElement(ORDER_CHANGE_BLOKER);
		changeBlock.appendChild(doc.createTextNode(firstRecord[10].trim()));
		customFieldsElement.appendChild(changeBlock);
		
		Element linechangesElement = doc.createElement(ORDER_LINE_CHANGES);		 
		orderHeaderElement.appendChild(linechangesElement);
		 

		for (String reqData[] : records) {
			
			Element lineChangeElement = doc.createElement(ORDER_LINE_CHANGE);
			linechangesElement.appendChild(lineChangeElement);
			
            
			Element lineNum = doc.createElement( LINE_NUM);
			lineNum.appendChild(doc.createTextNode(reqData[3].trim()));
			lineChangeElement.appendChild(lineNum);
			
			Element description = doc.createElement(DESC);
			description.appendChild(doc.createTextNode(reqData[7].trim()));
			lineChangeElement.appendChild(description);

			Element unitPrice = doc.createElement(PRICE);
			unitPrice.appendChild(doc.createTextNode(reqData[8].trim()));
			lineChangeElement.appendChild(unitPrice);

			Element customFieldsLineItemElement = doc.createElement(CUSTOM_FIELDS);
			lineChangeElement.appendChild(customFieldsLineItemElement);

			Element cdkRtCode = doc.createElement( CDK_RTS_CODE);
			cdkRtCode.appendChild(doc.createTextNode(reqData[4].trim()));
			customFieldsLineItemElement.appendChild(cdkRtCode);
			
			Element carLicensePlate = doc.createElement( CAR_LICENSE_PLATE);
			carLicensePlate.appendChild(doc.createTextNode(reqData[5].trim()));
			customFieldsLineItemElement.appendChild(carLicensePlate);

			Element vinNo = doc.createElement( VIP_VIN_NO);
			vinNo.appendChild(doc.createTextNode(reqData[6].trim()));
			customFieldsLineItemElement.appendChild(vinNo);
			
			Element cdkPosCompany = doc.createElement( CDK_POS_COMPANY);
			cdkPosCompany.appendChild(doc.createTextNode(reqData[2].trim()));
			customFieldsLineItemElement.appendChild(cdkPosCompany);
			
			Element cdkWipNumber = doc.createElement( CDK_WIP_NO);
			cdkWipNumber.appendChild(doc.createTextNode(reqData[11].trim()));
			customFieldsLineItemElement.appendChild(cdkWipNumber);

			Element cdkWipLine = doc.createElement( CDK_WIP_LINE);
			cdkWipLine.appendChild(doc.createTextNode(reqData[12].trim()));
			customFieldsLineItemElement.appendChild(cdkWipLine);


			Element currencyLineItem = doc.createElement(CURRENCY);
			lineChangeElement.appendChild(currencyLineItem);
			
			Element currencycode = doc.createElement(CODE);
			currencycode.appendChild(doc.createTextNode(reqData[9].trim()));
			currencyLineItem.appendChild(currencycode);
		}

	
	}
}