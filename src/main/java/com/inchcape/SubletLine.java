package com.inchcape;

public class SubletLine {
	private String coupaPoNo ="";
	private String posCompany ="";
	private String wipNo ="";
	private String wipLine ="";
	private String integrationStatus ="";
	private String integrationMsg ="";
	private String coupaPoNoFirstSeg = "";
	private String coupaPoNoSecondSeg = "";
	private String coupaPoNoThirdSeg = "";
	
	public String getCoupaPoNo() {
		return coupaPoNo;
	}
	public void setCoupaPoNo(String coupaPoNo) throws Exception {
		this.coupaPoNo = coupaPoNo;
		setCoupaNoDetails(coupaPoNo);
	}
	public String getPosCompany() {
		return posCompany;
	}
	public void setPosCompany(String posCompany) {
		this.posCompany = posCompany;
	}
	public String getWipNo() {
		return wipNo;
	}
	public void setWipNo(String wipNo) {
		this.wipNo = wipNo;
	}
	public String getWipLine() {
		return wipLine;
	}
	public void setWipLine(String wipLine) {
		this.wipLine = wipLine;
	}
	public String getIntegrationStatus() {
		return integrationStatus;
	}
	public void setIntegrationStatus(String integrationStatus) {
		this.integrationStatus = integrationStatus;
	}
	public String getIntegrationMsg() {
		return integrationMsg;
	}
	public void setIntegrationMsg(String integrationMsg) {
		this.integrationMsg = integrationMsg;
	}
	public String getCoupaPoNoFirstSeg() {
		return coupaPoNoFirstSeg;
	}
	public String getCoupaPoNoSecondSeg() {
		return coupaPoNoSecondSeg;
	}
	public String getCoupaPoNoThirdSeg() {
		return coupaPoNoThirdSeg;
	}
	
	private void setCoupaPoNoFirstSeg(String coupaPoNoFirstSeg) {
		this.coupaPoNoFirstSeg = coupaPoNoFirstSeg;
	}
	
	private void setCoupaPoNoSecondSeg(String coupaPoNoSecondSeg) {
		this.coupaPoNoSecondSeg = coupaPoNoSecondSeg;
	}
	
	private void setCoupaPoNoThirdSeg(String coupaPoNoThirdSeg) {
		this.coupaPoNoThirdSeg = coupaPoNoThirdSeg;
	}
	
	private void setCoupaNoDetails(String coupaPoNo) throws Exception {
		if (coupaPoNo != null) {
			String splitStr[] = coupaPoNo.split("-");
			if (splitStr.length ==3) {// e.g. 2995-1-4487				 
				setCoupaPoNoFirstSeg(splitStr[0]);
				setCoupaPoNoSecondSeg(splitStr[1]);
				setCoupaPoNoThirdSeg(splitStr[2]);
			}else{
				throw new Exception ("Wrong format of Coupa PO No:"+ coupaPoNo +"! The Sample format should be like 2995-1-4487");
			}
		}else{
			throw new Exception ("Coupa PO No should not be empty");
		}
	}	
}
