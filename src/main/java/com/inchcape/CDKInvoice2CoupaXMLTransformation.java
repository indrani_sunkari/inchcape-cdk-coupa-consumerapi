package com.inchcape;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class CDKInvoice2CoupaXMLTransformation {

	//private static final String CDK_INVOICE_STATUS = "CDKInvoiceStatus";

	private static final String SUBLET_BATCH = "SubletBatch";
	private static final String INTEGRATION_ID = "IntegrationId";
	private static final String ID = "Id";

	private static final String SUBLET_LINE = "SubletLine";
	private static final String COUPA_PO_NO = "CoupaPONo";
	private static final String POS_COMPANY = "POSCompany";
	private static final String WIP_NO = "WIPNo";
	private static final String WIP_LINE = "WIPLine";
	private static final String INTEGRATION_STATUS = "IntegrationStatus";
	private static final String INTEGRATION_MSG = "IntegrationMessage";

	public static void main(String args[]) throws Exception {
		String filename = "src/test/resources/input-po-status-endsync.xml";
	   
		String contents = new String(Files.readAllBytes(Paths.get(filename)));
		String resp = new CDKInvoice2CoupaXMLTransformation().transformToCoupaObjectSyncReq(contents);
		System.out.println(resp);
	}
  
    // This method is to transform CDK Invoice data to Coupa XML format
	public String transformToCoupaObjectSyncReq(String subletBatchReqXML) throws Exception {
		String result = "";
		List<SubletBatch> subletBatchList = getInoviceStatusData(subletBatchReqXML);
		CoupaXMLGenerator xmlGenerator = new CoupaXMLGenerator();
		result = xmlGenerator.generateCoupaObjSyncReq(subletBatchList);
		return result;
	}
     
	
	// this method is to get CDK Invoice data, which will be grouped by first segment of CoupaPONo and generate related multiple SubletBatch 
	private List<SubletBatch> getInoviceStatusData(String subletBatchReqXML) throws Exception {
		if (subletBatchReqXML == null) {
			return null;
		}
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		InputStream targetXmlStream = new ByteArrayInputStream(subletBatchReqXML.getBytes());
		Document document = builder.parse(targetXmlStream);
		document.getDocumentElement().normalize();

		NodeList batchList = document.getElementsByTagName(SUBLET_BATCH);
		Map<String,SubletBatch > subletBatchMap = new HashMap<String,SubletBatch >();
		
		for (int temp = 0; temp < batchList.getLength(); temp++) {
			 
			Node node = batchList.item(temp);
			Element element = (Element) node;
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				
				String integrationId = "";
				String id = "";		
				NodeList nodeTmp = element.getElementsByTagName(INTEGRATION_ID);
				for (int i = 0, j = nodeTmp.getLength(); i < j; i++) {
					integrationId = nodeTmp.item(0).getTextContent();					
				}

				nodeTmp = element.getElementsByTagName(ID);
				for (int i = 0, j = nodeTmp.getLength(); i < j; i++) {
					id = nodeTmp.item(0).getTextContent();					
				}

			
				nodeTmp = element.getElementsByTagName(SUBLET_LINE);
				
				for (int i = 0, j = nodeTmp.getLength(); i < j; i++) {
					Element elementSubletLine = (Element) nodeTmp.item(i);
					SubletLine subletLine = new SubletLine();				

					NodeList nodeTmpOjbectLine = elementSubletLine.getElementsByTagName(COUPA_PO_NO);
					for (int ii = 0, jj = nodeTmpOjbectLine.getLength(); ii < jj; ii++) {
						subletLine.setCoupaPoNo(nodeTmpOjbectLine.item(0).getTextContent());
					}

					nodeTmpOjbectLine = elementSubletLine.getElementsByTagName(POS_COMPANY);
					for (int ii = 0, jj = nodeTmpOjbectLine.getLength(); ii < jj; ii++) {
						subletLine.setPosCompany(nodeTmpOjbectLine.item(0).getTextContent());						
					}

					nodeTmpOjbectLine = elementSubletLine.getElementsByTagName(WIP_NO);
					for (int ii = 0, jj = nodeTmpOjbectLine.getLength(); ii < jj; ii++) {
						subletLine.setWipNo(nodeTmpOjbectLine.item(0).getTextContent());
					}

					nodeTmpOjbectLine = elementSubletLine.getElementsByTagName(WIP_LINE);
					for (int ii = 0, jj = nodeTmpOjbectLine.getLength(); ii < jj; ii++) {
						subletLine.setWipLine(nodeTmpOjbectLine.item(0).getTextContent());
					}

					nodeTmpOjbectLine = elementSubletLine.getElementsByTagName(INTEGRATION_STATUS);
					for (int ii = 0, jj = nodeTmpOjbectLine.getLength(); ii < jj; ii++) {
						subletLine.setIntegrationStatus(nodeTmpOjbectLine.item(0).getTextContent());
					}

					nodeTmpOjbectLine = elementSubletLine.getElementsByTagName(INTEGRATION_MSG);
					for (int ii = 0, jj = nodeTmpOjbectLine.getLength(); ii < jj; ii++) {
						subletLine.setIntegrationMsg(nodeTmpOjbectLine.item(0).getTextContent());
					}
					addSubletLine2SubletBatchTab(subletBatchMap,subletLine,id,integrationId);
				}				 
				 
			} // end if (node.getNodeType() == Node.ELEMENT_NODE) {
		}
		
		List<SubletBatch>  subletBatchList = new ArrayList<SubletBatch>();
		for(Entry<String, SubletBatch> entry: subletBatchMap.entrySet() ) {
			subletBatchList.add(entry.getValue());
	    }
		return subletBatchList;
	}
	
	private void addSubletLine2SubletBatchTab (Map<String,SubletBatch > subletBatchMap, SubletLine subletLine, String id, String integrationId){		
		String coupaPoNoFirstSeg = subletLine.getCoupaPoNoFirstSeg();
		SubletBatch subletBatch   = (SubletBatch) subletBatchMap.get(coupaPoNoFirstSeg);
		if ( subletBatch != null){
			subletBatch.getSubletLine().add(subletLine);
		}else{
			SubletBatch batch = new SubletBatch();
			batch.setId(id);
			batch.setIntegrationId(integrationId);
			List<SubletLine> subletLineList = new ArrayList<SubletLine>();
			subletLineList.add(subletLine);
			batch.setSubletLine(subletLineList);
			subletBatchMap.put(coupaPoNoFirstSeg, batch);
		}
	}
	

}
