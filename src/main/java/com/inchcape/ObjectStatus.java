package com.inchcape;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ObjectStatus {

	public static String NEW_LINE_SEPARATOR = System.getProperty("line.separator");
	
	private final static String BUSINESS_ID = "business-id";
	private final static String CDK_POS_COMPANY = "cdk-pos-company";
	private final static String CDK_WIP_NUMBER = "cdk-wip-number";
	private final static String CDK_WIP_LINE = "cdk-wip-line";
	private final static String STATUS = "status";
	private final static String INTEGRATION_MESSAGE = "integration-message";
	private final static String RESPONSE = "object-status";
	private final static String OBJECT_LINE = "object-line";
	
	

	public static void main(String args[]) throws Exception {
		String filename = "src/test/resources/objectPOStatusMulti.xml";
	String contents = new String(Files.readAllBytes(Paths.get(filename)));
	 

		
		String resp = new ObjectStatus().transformToSubletBatchXMLDataStr(contents);
		System.out.println(resp);
	}
    
	public String transformToSubletBatchXMLDataStr(String requisitionReqXML) throws Exception {
		List<String> respList = transformToSubletBatchXMLData(requisitionReqXML);
        StringBuffer result = new StringBuffer() ;
        result.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        result.append(NEW_LINE_SEPARATOR);
		for (String resp : respList) {
			 result.append(resp);			 
		}		
		return result.toString();
	}
	
	public List<String> transformToSubletBatchXMLData(String requisitionReqXML) throws Exception {
		List<RequestPayload> reqPayload = getReqPayloadData(requisitionReqXML);

		ObjectStatusXMLGenerator generator = new ObjectStatusXMLGenerator();
		List<String> respList = generator.transformReqToResponse(reqPayload);
		return respList;
	}

	private List<RequestPayload> getReqPayloadData(String requestXML) throws Exception {
		
		List<RequestPayload> reqPayloadList = new ArrayList();
		if (requestXML == null){
			return reqPayloadList;
		}
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		InputStream targetXmlStream = new ByteArrayInputStream(requestXML.getBytes());
		Document document = builder.parse(targetXmlStream);
		document.getDocumentElement().normalize();
		NodeList nList = document.getElementsByTagName(RESPONSE);
		RequestPayload requestPayload = new RequestPayload();

		

		for (int temp = 0; temp < nList.getLength(); temp++) {
			Node node = nList.item(temp);

			if (node.getNodeType() == Node.ELEMENT_NODE) {

				Element element = (Element) node;
				NodeList nodeTmp = element.getElementsByTagName(CDK_POS_COMPANY);
				for (int i = 0, j = nodeTmp.getLength(); i < j; i++) {
					requestPayload.setPosCompany(nodeTmp.item(0).getTextContent());
				}

				nodeTmp = element.getElementsByTagName(STATUS);
				for (int i = 0, j = nodeTmp.getLength(); i < j; i++) {
					requestPayload.setStatus(nodeTmp.item(0).getTextContent());
				}

				nodeTmp = element.getElementsByTagName(BUSINESS_ID);
				for (int i = 0, j = nodeTmp.getLength(); i < j; i++) {
					requestPayload.setBusinessId(nodeTmp.item(0).getTextContent());
				}

				nodeTmp = element.getElementsByTagName(INTEGRATION_MESSAGE);
				for (int i = 0, j = nodeTmp.getLength(); i < j; i++) {
					requestPayload.setIntegraitonMsg(nodeTmp.item(0).getTextContent());
				}

				nodeTmp = element.getElementsByTagName(OBJECT_LINE);
				List<ObjectLine> objectLineList = new ArrayList();
				for (int i = 0, j = nodeTmp.getLength(); i < j; i++) {
					Element elementOjbectLine = (Element) nodeTmp.item(i);
					NodeList nodeTmpOjbectLine = elementOjbectLine.getElementsByTagName(CDK_WIP_NUMBER);

					ObjectLine objectLine = new ObjectLine();
					for (int ii = 0, jj = nodeTmpOjbectLine.getLength(); ii < jj; ii++) {
						objectLine.setWipNumber(nodeTmpOjbectLine.item(0).getTextContent());
					}

					nodeTmpOjbectLine = elementOjbectLine.getElementsByTagName(CDK_WIP_LINE);
					for (int ii = 0, jj = nodeTmpOjbectLine.getLength(); ii < jj; ii++) {
						objectLine.setWipLine(nodeTmpOjbectLine.item(0).getTextContent());
					}
					objectLineList.add(objectLine);
				}
				requestPayload.setObjectLine(objectLineList);

				
				reqPayloadList.add(requestPayload);
			}
		}
		return reqPayloadList;
	}

}
