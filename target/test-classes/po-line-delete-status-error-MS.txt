<batch-object>
	<batch-header>
		<object-type>po-line-delete-status</object-type>
		<batch-size>1</batch-size>
		<batch-id>d622f8d0-1bb8-11ea-a695-06d2e378e46a</batch-id>
		<filename>sg-bmsl-cdk-po-line-delete-status-20191211015208.xml</filename>
	</batch-header>
	<object-collection>
		<object>
			<response>
				<status>error</status>
				<transactionId>c94fe3b8-81e0-4a35-a588-78ed4aa55627</transactionId>
				<externalId>d622f8d0-1bb8-11ea-a695-06d2e378e46a</externalId>
				<summary>Bad request</summary>
				<description>PO ID : 2960; Bad request. Coupa returned a 400 error. Coupa response : &lt;?xml version="1.0" encoding="UTF-8"?>
&lt;errors>
  &lt;error>
    &lt;![CDATA[Record not editable.]]&gt;
  &lt;/error>
&lt;/errors>
				</description>
				<requestTimeStamp>2019-12-11T01:55:06.043Z</requestTimeStamp>
				<payload>
					<order-header>
						<id>2960</id>
						<order-lines>
							<order-line>
								<id>4450</id>
								<_delete>true</_delete>
							</order-line>
						</order-lines>
					</order-header>
				</payload>
			</response>
		</object>
	</object-collection>
</batch-object>